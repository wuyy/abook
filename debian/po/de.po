#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: abook 0.5.0\n"
"Report-Msgid-Bugs-To: abook@packages.debian.org\n"
"POT-Creation-Date: 2006-08-18 10:38-0500\n"
"PO-Revision-Date: 2006-08-18 10:42-0500\n"
"Last-Translator: Rhonda D'Vine <rhonda@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want to enable abook system wide for mutt?"
msgstr "Wollen Sie abook Systemweit in mutt aktivieren?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"abook offers the possibility to be used as query backend from within mutt.  "
"If you acknowledge this question the package will create an /etc/Muttrc.d/"
"abook.rc file that enables querying the abook database and adding mail "
"addresses to abook with pressing \"A\" from pager mode."
msgstr ""
"abook bietet die Möglichkeit, als externe Adressenabfrage in mutt verwendet "
"zu werden. Falls Sie dieser Frage zustimmen, wird das Paket eine /etc/Muttrc."
"d/abook.rc-Datei erstellen, die die Abfrage der abook-Datenbank und das "
"Hinzufügen von Adressen in abook durch Drücken von »A« im Pager-Modus "
"aktiviert."
